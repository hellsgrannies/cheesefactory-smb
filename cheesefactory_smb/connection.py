# cheesefactory_smb/connection.py

import logging
from typing import Union, Optional
from smb.SMBConnection import SMBConnection

from cheesefactory_smb.log import CfSmbLog

logger = logging.getLogger(__name__)


class CfSmbConnection:

    def __init__(self):

        self.smb = None

        self.user: Optional[str] = None
        self.password: Optional[str] = None
        self.client_name: Optional[str] = None
        self.server_name: Optional[str] = None
        self.domain: Optional[str] = None
        self.server_ip: Optional[str] = None
        self.server_port: Optional[str] = None
        self.use_ntlm_v2: Optional[bool] = None
        self.is_direct_tcp: Optional[bool] = None

        self.log = CfSmbLog()  # Connection and transfer metrics

    #
    #  CLASS METHODS
    #

    @classmethod
    def connect(cls, user: str = None, password: str = None, client_name: str = None, server_name: str = None,
                domain: str = None, server_ip: str = None, server_port: Union[str, int] = '445',
                use_ntlm_v2: bool = True, is_direct_tcp: bool = True):
        """Connect to a remote SMB/CIFS server and perform file operations.

        Args:
            user: SMB server username.
            password: SMB server password.
            client_name: Client machine name.
            server_name: SMB server name.
            domain: Domain used for authentication.
            server_ip: SMB server IP address or hostname.
            server_port: SMB server port.
            use_ntlm_v2: Use NTLMv1 (False) or NTLMv2 (True) for authentication.
            is_direct_tcp: Use NetBIOS over TCP/IP (False) or direct TCP/IP (True) for communication.

        Notes:
        CfSmb --> CfSmbTransfer --> CfSmbUtilities --> CfSmbConnection
        """
        connection = cls()

        connection.domain = domain
        connection.user = user
        connection.log.user = f'{domain}\\{user}'

        connection.log.password = connection.password = password
        connection.log.client_name = connection.client_name = client_name
        connection.log.host = connection.server_name = server_name
        connection.log.ip = connection.server_ip = server_ip
        connection.log.port = connection.server_port = str(server_port)
        connection.log.use_ntlm_v2 = connection.use_ntlm_v2 = use_ntlm_v2
        connection.log.is_direct_tcp = connection.is_direct_tcp = is_direct_tcp

        connection.log.connection_ok = False

        connection.smb = SMBConnection(
            username=user,
            password=password,
            my_name=client_name,
            remote_name=server_name,
            domain=domain,
            use_ntlm_v2=use_ntlm_v2,
            is_direct_tcp=is_direct_tcp
        )
        connection.smb.connect(
            ip=server_ip,
            port=int(server_port)
        )

        return connection

    #
    # PROPERTIES
    #

    @property
    def status(self):
        """Retrieve connection status.

        status() for cheesefactory programs should return True/False along with any reasons.

        Returns:
            True, if live. False, if not live.
            Additional info, such as error codes. (not implemented)
        """
        reply = self.smb.echo('Connection is good.')
        if reply == 'Connection is good.':
            return True, 'Connection open.'
        else:
            return False, str(reply)

    #
    # PUBLIC METHODS
    #

    def close(self):
        """Close the SMB session."""
        self.smb.close()
