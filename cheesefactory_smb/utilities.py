# cheesefactory_smb/utilities.py

# SMB utilities

# import fnmatch
import logging
# import re

from smb.base import SharedDevice, SharedFile

from .connection import CfSmbConnection
from pathlib import Path
from typing import List

logger = logging.getLogger(__name__)


class CfSmbUtilities(CfSmbConnection):
    """Basic utilities for use with an SMB connection. Does not include any file transfer methods.

    Notes:
        CfSmb --> CfSmbTransfer --> CfSmbUtilities --> CfSmbConnection
    """

    def __init__(self):
        super().__init__()
        self.share = None  # Active share. Used by methods, if set.

    def _get_share(self, share: str) -> str:
        """Determine the share name to use.

        Args:
            share: The name to compare to the class attribute, self.share
        Returns:
            class attribute if no share given. Raises an error if no valid value found.
        """
        if share is None:
            if self.share is None:
                raise ValueError('No share defined!')
            else:
                return self.share
        else:
            return share

    #
    # PUBLIC METHODS
    #

    def exists(self, path: str, share: str = None) -> bool:
        """Does a remote file or directory exist?

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        Returns:
            True if file or directory exists. False if it does not.
        """
        try:
            share = self._get_share(share)
            self.stat(share=share, path=path)
        except FileNotFoundError:  # TODO: Not sure what the error is when does not exist
            return False
        else:
            return True

    # def find_files_by_glob(self, glob_filter: str = '*', recursive_search: bool = False,
    #                        remote_dir: str = '.') -> List[str]:
    #     """Create a list of remote files to download based on glob, then download.
    #
    #     Creates a recursive list of files and directories in remote_dir and filters by glob_filter.
    #
    #     Args:
    #         glob_filter:
    #         recursive_search:
    #         remote_dir:
    #     """
    #     if not isinstance(glob_filter, str):
    #         raise ValueError('glob_filter != str type')
    #     if not isinstance(recursive_search, bool):
    #         raise ValueError('recursive_search != bool type')
    #     if not isinstance(remote_dir, str):
    #         raise ValueError('remote_dir != str type')
    #
    #     # Make file list
    #     try:
    #         files = self.list_dir(remote_dir, recursive=recursive_search)
    #         logger.debug(f'Unfiltered remote files: {str(files)}')
    #     except FileNotFoundError as e:
    #         raise FileNotFoundError(f'remote_dir does not exist on remote server ({e})')
    #
    #     # Identify items that do not match the glob_filter
    #     hit_list = []  # Files to remove from list
    #     for file in files:
    #         if fnmatch.fnmatch(file, glob_filter) is False:
    #             hit_list.append(file)
    #     # Remove the unmatched files from the file list
    #     for hit in hit_list:
    #         files.remove(hit)
    #
    #     if len(files) == 0:
    #         logger.debug(f'No files found after filter ({glob_filter}): {str(files)}')
    #
    #     return files
    #
    # def find_files_by_regex(self, recursive_search: bool = False, regex_filter: str = r'^',
    #                         remote_dir: str = '.') -> List[str]:
    #     """Create a list of remote files to download based on a regex, then download.
    #
    #     Creates a recursive list of files and directories in remote_dir and filters using re.search().
    #
    #     Args:
    #         recursive_search:
    #         regex_filter:
    #         remote_dir: Remote/source path and filename.
    #     """
    #     if not isinstance(regex_filter, str):
    #         raise ValueError('regex_filter != str type')
    #     if not isinstance(remote_dir, str):
    #         raise ValueError('remote_dir != str type')
    #
    #     # Make file list
    #     try:
    #         files = self.list_dir(remote_dir, recursive=recursive_search)
    #         logger.debug(f'Unfiltered remote files: {str(files)}')
    #     except FileNotFoundError as e:
    #         raise FileNotFoundError(f'remote_dir does not exist on remote server ({e})')
    #
    #     # Identify items that do not match the glob_filter
    #     hit_list = []  # Files to remove from list
    #
    #     try:
    #         regex_object = re.compile(regex_filter)
    #     except re.error as e:
    #         logger.debug(f'Bad regex ({regex_filter}): {str(e)}')
    #         raise ValueError(f'Bad regex pattern ({regex_filter}): {str(e)}')
    #
    #     for file in files:
    #         result = regex_object.search(file)
    #         if result is None:
    #             hit_list.append(file)
    #
    #     # Remove the unmatched files from the file list
    #     for hit in hit_list:
    #         files.remove(hit)
    #
    #     if len(files) == 0:
    #         logger.debug(f'No files found after filter ({regex_filter}): {str(files)}')
    #
    #     return files

    def is_dir(self, path: str, share: str = None) -> bool:
        """Is the current remote path a directory?

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        Returns:
            True if it is, False if it ain't.
        """
        share = self._get_share(share)
        result = self.stat(share=share, path=path).isDirectory
        return result

    def is_file(self, path: str, share: str = None):
        """Is the current remote path a file?

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        Returns:
            True if the path is a file. False if it is not.
        """
        share = self._get_share(share)
        result = self.stat(share=share, path=path).isNormal
        return result

    def list_dir(self, share: str = None, path_pattern: str = '*') -> List[str]:
        """Return a list of remote directory contents.

        Args:
            share: SMB file share. Will use self.share if not set.
            path_pattern: File path pattern relative to the share name. Use asterisk (*) for wildcard.
        Returns:
            A list of directory contents.
        """
        share = self._get_share(share)
        dir_list = []
        results = self.smb.listPath(service_name=share, path=path_pattern)
        for result in results:
            dir_list.append(result.filename)
        return dir_list

    def list_dir_stat(self, share: str = None, path_pattern: str = '*') -> List[SharedFile]:
        """Return a list of remote directory contents along with stat data in the form of pysmb SharedFile objects.

        Attributes available:

        create_time (float): Number of seconds since 1970-01-01 00:00:00 to file creation time.
        last_access_time (float): Number of seconds since 1970-01-01 00:00:00 to last access time.
        last_write_time (float): Number of seconds since 1970-01-01 00:00:00 to modification time.
        last_attr_change_time (float): Number of seconds since 1970-01-01 00:00:00 to attribute change time.
        file_size (int): File size in bytes.
        alloc_size (int): Number of bytes allocated to store this file.
        file_attributes: A SMB_EXT_FILE_ATTR integer value.
        short_name (str): Unicode string containing the short filename (usually in 8.3 notation).
        filename (str): Unicode string containing the long filename.
        file_id (int) : File reference number for the file.

        Args:
            share: SMB file share. Will use self.share if not set.
            path_pattern: File path pattern relative to the share name. Use asterisk (*) for wildcard.
        Returns:
            A list of pysmb SharedFile objects.
        """
        share = self._get_share(share)
        results = self.smb.listPath(service_name=share, path=path_pattern)
        return results

    def list_shares(self) -> List[SharedDevice]:
        """Return a list of remote directory contents.

        Returns:
            A list of shared devices.
        """
        dir_list = []
        results = self.smb.listShares()
        for result in results:
            if result.type in [SharedDevice.DISK_TREE, SharedDevice.IPC]:  # Exclude PRINT_QUEUE and COMM_DEVICE
                dir_list.append(result.name)
        return dir_list

    def mkdir(self, path: str, share: str = None):
        """Create a remote directory.

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        """
        share = self._get_share(share)
        self.smb.createDirectory(service_name=share, path=path)

    @staticmethod
    def octal_to_decimal(number: int) -> int:
        """Convert an octal number (given as int) to a decimal.

        Args:
            number: Octal number (as an int).
        Returns:
            Converted number.
        """
        return int(str(number), 8)

    def remove_dir(self, path: str, share: str = None):
        """Delete a remote file or directory.

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        """
        share = self._get_share(share)
        self.smb.deleteDirectory(service_name=share, path=path)

    def remove_file_pattern(self, path_pattern: str, share: str = None):
        """Delete remote files using a pattern. Wildcard character is asterisk (*).

        Args:
            share: SMB file share. Will use self.share if not set.
            path_pattern: File path relative to the share name.
        """
        share = self._get_share(share)
        self.smb.deleteFiles(service_name=share, path_file_pattern=path_pattern, delete_matching_folders=False)

    def rename(self, old_path: str, new_path: str, share: str = None):
        """Rename a remote file or folder.

        Args:
            share: SMB file share. Will use self.share if not set.
            old_path: Existing name of file or directory.
            new_path: New name for the file or directory.
        """
        share = self._get_share(share)
        self.smb.rename(service_name=share, old_path=old_path, new_path=new_path)

    def size(self, path: str, share: str = None) -> int:
        """Find remote file size.

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        Returns:
            File size
        """
        # todo: write test
        share = self._get_share(share)
        return self.stat(share=share, path=path).file_size

    def size_match(self, local_path: str, remote_path: str, remote_size: int = None, share: str = None) -> bool:
        """Determine if the sizes of a remote and a local file match.

        Args:
            local_path: Local file.
            remote_path: Remote file.
            remote_size: Remote file size. If None, do a stat and figure it out.
            share: SMB file share. Will use self.share if not set.
        Returns:
            True, if match. False, if no match.
        """
        # TODO: Make it clearer that size match can be against a remote file or given size.
        share = self._get_share(share)
        if remote_size is None:
            remote_size = self.size(share=share, path=remote_path)

        local_size = Path(local_path).stat().st_size
        logger.debug(f'local_size: {local_size} <-> remote_size: {remote_size}')
        if local_size != remote_size:
            return False
        else:
            return True

    def stat(self, path: str, share: str = None) -> SharedFile:
        """Retrieve information about a remote file. Mimics Python's os.stat structure.

        Attributes available:

        create_time (float): Number of seconds since 1970-01-01 00:00:00 to file creation time.
        last_access_time (float): Number of seconds since 1970-01-01 00:00:00 to last access time.
        last_write_time (float): Number of seconds since 1970-01-01 00:00:00 to modification time.
        last_attr_change_time (float): Number of seconds since 1970-01-01 00:00:00 to attribute change time.
        file_size (int): File size in bytes.
        alloc_size (int): Number of bytes allocated to store this file.
        file_attributes: A SMB_EXT_FILE_ATTR integer value.
        short_name (str): Unicode string containing the short filename (usually in 8.3 notation).
        filename (str): Unicode string containing the long filename.
        file_id (int) : File reference number for the file.

        Args:
            share: SMB file share. Will use self.share if not set.
            path: File path relative to the share name.
        Returns:
            Attributes about the given file.
        """
        share = self._get_share(share)
        attributes = self.smb.getAttributes(service_name=share, path=path)
        return attributes
