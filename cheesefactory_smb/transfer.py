# cheesefactory_smb/transfer.py

import hashlib
import logging
import os
from pathlib import Path
from cheesefactory_smb.log import CfSmbLogTransfer
from cheesefactory_smb.utilities import CfSmbUtilities

logger = logging.getLogger(__name__)


class CfSmbTransfer(CfSmbUtilities):

    def __init__(self):
        """File transfer.

        Notes:
            CfSmb --> CfSmbTransfer --> CfSmbUtilities --> CfSmbConnection
        """
        super().__init__()

    def transfer(self, remote_path: str, local_path: str, action: str = None, log_checksum: bool = False,
                 remove_source: bool = False, share: str = None) -> dict:
        """GET a single file.

        Args:
            action: GET or PUT
            local_path: Destniation file path relative to the share name.
            log_checksum: Calculate file SHA256 checksum and log resulting value.
            remote_path: Source file path relative to the share name.
            remove_source: Remove the source file.
            share: Remote SMB share. Will use self.share if not set.
        Returns:
            A log of the transfer.
        """
        if action not in ('GET', 'PUT'):
            raise ValueError("action != 'GET' or 'PUT'")
        share = self._get_share(share)

        transfer_log = CfSmbLogTransfer
        transfer_log.action = action
        transfer_log.action_ok = False
        transfer_log.client = 'CfSmb'
        transfer_log.local_path = local_path
        transfer_log.remote_path = remote_path
        transfer_log.size_match = True
        transfer_log.size_match_ok = False

        transfer_log.note = 'Stat source file.'
        if action == 'GET':
            transfer_log.size = self.smb.stat(share=share, path=remote_path).file_size
        else:  # action == 'PUT'
            transfer_log.size = os.stat(local_path).st_size

        transfer_log.note = 'Ensuring destination dir exists.'
        if action == 'GET':
            destination_dir = Path(local_path).parent
            destination_dir.mkdir(exist_ok=True, parents=True)
        else:  # action == 'PUT'
            pass  # TODO: Do not yet know behavior.
            # destination_dir = str(Path(remote_path).parent)
            # try:
            #     self.sftp.stat(destination_dir)
            # except IOError:  # If remote directory does not exist...
            #     try:
            #         self.sftp.mkdir(destination_dir)
            #     except IOError as e:
            #         raise IOError(f'Unable to create remote directory: {destination_dir} ({e})')

        if action == 'GET':
            transfer_log.note = 'Downloading file.'
            destination_data = open(local_path, 'wb')  # Will overwrite if exist
            self.smb.retrieveFile(
                service_name=share,
                path=remote_path,
                file_obj=destination_data
            )

        else:  # action == 'PUT'
            transfer_log.note = 'Uploading file.'
            source_data = open(local_path, 'rb')
            self.smb.storeFile(
                service_name=share,
                path=remote_path,
                file_obj=source_data
            )  # TODO: confirm does filesize stat

        if log_checksum is True:
            transfer_log.sha256_checksum = self.sha256_checksum(local_path)

        transfer_log.action_ok = True
        transfer_log.size_match_ok = True  # TODO: Make sure this is really happening.

        if remove_source is True:
            transfer_log.note = 'Removing source.'
            transfer_log.remove_source_ok = False
            if action == 'GET':
                self.smb.remove_file_pattern(share=share, path_pattern=remote_path)
            else:  # action == 'PUT'
                Path(local_path).unlink()
            transfer_log.remove_source_ok = True

        transfer_log.note = ''
        transfer_log.action_ok = True
        self.log.transfers.append(transfer_log)

        return transfer_log.as_dict()

    @staticmethod
    def sha256_checksum(file_path: str = None) -> str:
        """Calculate SHA256 checksum for a file."""
        blocksize = 65536  # blocksize limits memory usage.
        sha256 = hashlib.sha256()

        with open(file_path, 'rb') as fp:
            for block in iter(lambda: fp.read(blocksize), b''):
                sha256.update(block)
        return sha256.hexdigest()

    @staticmethod
    def transferred_local_files(log: dict) -> List[str]:
        """Get a list of transferred local files (after any renaming)."""
        transferred_files = []
        for transfer in log['sftp_transfers']:
            if transfer['renamed_local_path'] is not None:
                transferred_files.append(transfer['renamed_local_path'])
            else:
                transferred_files.append(transfer['local_path'])
        return transferred_files

    @staticmethod
    def transferred_remote_files(log: dict) -> List[str]:
        """Get a list of transferred remote files (after any renaming)."""
        transferred_files = []
        for transfer in log['sftp_transfers']:
            if transfer['renamed_remote_path'] is not None:
                transferred_files.append(transfer['renamed_remote_path'])
            else:
                transferred_files.append(transfer['remote_path'])
        return transferred_files

